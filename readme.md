# Discord Battle.net Bot

This is a bot designed around the Battle.net World of Warcraft API. It will help a discord guild be able to make quick calls to the WoW API without opening a website and provide them with valuable information. This bot is written in the new dotnet core framework and can be ran on any operating system.

Status: This is still in early development phases.

## Setup

After making a Bot Application for discord, you will then need to set environment variables for the project to properly run. The following variables are available below.

 Variable | Description | Required | Default
 ---- | ---- | ---- | ----
DISCORD_BOT_TOKEN | The bot application token | Yes | Null
DISCORD_DEFAULT_WOW_REALM | The default realm for commands | No | stormrage
BATTLE_NET_API_KEY | Your battlenet API key | Yes | Null
BATTLE_NET_ENDPOINT | The battlenet api you wish to hit | No | https://us.api.battle.net/wow/
BATTLE_NET_LOCALE | The battlenet locale to send with requests | No | en-us

## Run

After installing dotnet core framework you should be able to run the dotnet command against the project's DLL. 

`dotnet Discord.Battlenet.Bot.dll`
using Battlenet.Models;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Battlenet.Client
{
    public class BattlenetClient : IBattlenetClient
    {
        private JsonSerializerSettings _jsonSettings;
        private string _apiToken;
        private string _locale;
        private HttpClient Client { get; } = null;
        
        public BattlenetClient(IOptions<BattlenetClientOptions> options)
        {
            if(options == null)
            {
                throw new ArgumentNullException(nameof(options));
            }

            if(string.IsNullOrEmpty(options.Value.ApiKey))
            {
                throw new ArgumentNullException(nameof(options.Value.ApiKey));
            }

            if(string.IsNullOrEmpty(options.Value.BattlenetEndpoint))
            {
                throw new ArgumentNullException(nameof(options.Value.BattlenetEndpoint));
            }

            if(string.IsNullOrEmpty(options.Value.Locale))
            {
                throw new ArgumentNullException(nameof(options.Value.Locale));
            }

            if(!Uri.IsWellFormedUriString(options.Value.BattlenetEndpoint, UriKind.Absolute))
            {
                throw new UriFormatException(nameof(options.Value.BattlenetEndpoint));
            }

            if(!options.Value.BattlenetEndpoint.EndsWith("/"))
            {
                options.Value.BattlenetEndpoint += "/";
            }

            _apiToken = options.Value.ApiKey;
            _locale = options.Value.Locale;
            _jsonSettings = Serialization.SerializerSettings.BattlnetSerializerSettings;

            Client = new HttpClient();
            Client.BaseAddress = new Uri(options.Value.BattlenetEndpoint);
        }

        public async Task<Character> GetCharacterProfileAsync(string realm, string characterName, IEnumerable<CharacterFields> fields = null)
        {
            string fieldsQuery = GetFieldsQuery(fields);
            string path = string.Format("character/{0}/{1}?fields={2}", realm, characterName, fieldsQuery);

            var response = await SendAsync(path, HttpMethod.Get);
        
            if(response.IsSuccessStatusCode)
            {
                return JsonConvert.DeserializeObject<Character>(await response.Content.ReadAsStringAsync(), _jsonSettings);
            }
            else
            {
                return null;
            }
        }

        private async Task<HttpResponseMessage> SendAsync(string path, HttpMethod method)
        {
            path = AddApiKeyLocaleParameters(path);
            
            using(HttpRequestMessage request = new HttpRequestMessage(method, path))
            {
                return await Client.SendAsync(request);
            }
        }

        private string GetFieldsQuery(IEnumerable<CharacterFields> fields)
        {
            if(fields == null)
            {
                return "";
            }

            return string.Join("%2C", fields);
        }

        private string AddApiKeyLocaleParameters(string path)
        {
            string query = string.Format("apikey={0}&locale={1}", _apiToken, _locale);

            if(path.Contains("?"))
            {
                return path + "&" + query;
            }
            else
            {
                return path + "?" + query;
            }
        }
    }
}
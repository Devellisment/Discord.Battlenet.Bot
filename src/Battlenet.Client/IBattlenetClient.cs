using System.Collections.Generic;
using System.Threading.Tasks;
using Battlenet.Models;

namespace Battlenet.Client
{
    public interface IBattlenetClient
    {
        Task<Character> GetCharacterProfileAsync(string realm, string characterName, IEnumerable<CharacterFields> fields = null);
    }
}
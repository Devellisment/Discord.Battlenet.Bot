namespace Battlenet.Client
{
    public class BattlenetClientOptions
    {
        public string ApiKey { get; set; }

        public string BattlenetEndpoint { get; set; }

        public string Locale { get; set; }
    }
}
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace Battlenet.Client.Serialization
{
    public static class SerializerSettings
    {
        public static readonly JsonSerializerSettings BattlnetSerializerSettings;
        
        static SerializerSettings()
        {
            BattlnetSerializerSettings = InitializeBattlenetSerializerSettings(new CamelCasePropertyNamesContractResolver());
        }

        private static JsonSerializerSettings InitializeBattlenetSerializerSettings(IContractResolver resolver)
        {
            var settings =  new JsonSerializerSettings()
            {
                ContractResolver = resolver,
                DateFormatHandling = DateFormatHandling.MicrosoftDateFormat
            };
            
            settings.Converters.Add(new UnixDateTimeConverter());

            return settings;
        }
    }
}
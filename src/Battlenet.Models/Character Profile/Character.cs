using System;

namespace Battlenet.Models
{
    public class Character
    {
        // Basic Character Profile
        public string Name { get; set; }

        public string Realm { get; set; }

        public string Battlegroup { get; set; }

        public ClassType Class { get; set; }

        public RaceType Race { get; set; }

        public GenderType Gender { get; set; }
        
        public int Level { get; set; }

        public int AchievementPoints { get; set; }

        public string Thumbnail { get; set; }

        public string CalcClass { get; set; }

        public FactionType Faction { get; set; }
        
        public int TotalHonorableKills { get; set; }

        // Additional Character Information
        public CharacterItemViewModel Items { get; set; }
        
        public CharacterProgressionViewModel Progression { get; set; }

        public DateTimeOffset LastModified { get; set; }
    }
}
using System.Collections.Generic;

namespace Battlenet.Models
{
    public class Raid
    {
        public int Id { get; set; }
        
        public string Name { get; set; }

        public int Lfr { get; set; }

        public int Normal { get; set; }

        public int Heroic { get; set; }

        public int Mythic { get; set; }

        public IEnumerable<BossKills> Bosses { get; set; }
    }
}
using System;

namespace Battlenet.Models
{
    public class BossKills
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public int LfrKills { get; set; }

        public DateTimeOffset LfrTimestamp { get; set; }

        public int NormalKills { get; set; }

        public DateTimeOffset NormalTimestamp { get; set; }

        public int HeroicKills { get; set; }

        public DateTimeOffset HeroicTimestamp { get; set; }

        public int MythicKills { get; set; }

        public DateTimeOffset MythicTimestamp { get; set; }
    }
}
namespace Battlenet.Models
{
    public enum GenderType
    {
        Male = 0,        
        Female = 1
    }
}
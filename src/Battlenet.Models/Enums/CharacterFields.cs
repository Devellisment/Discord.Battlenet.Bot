namespace Battlenet.Models
{
    public enum CharacterFields
    {
        achievements,
        appearance,
        feed,
        guild,
        hunterPets,
        items,
        mounts,
        pets,
        petSlots,
        professions,
        progression,
        pvp,
        quests,
        reputation,
        statistics,
        stats,
        talents,
        titles,
        audit
    }
}
namespace Battlenet.Models
{
    public enum FactionType
    {
        Alliance = 0,
        Horde = 1
    }
}
namespace Battlenet.Models
{
    public class Class
    {
        public int Id { get; set; }

        public int Mask { get; set; }

        public string PowerType { get; set; }

        public string Name { get; set; }
    }
}
namespace Battlenet.Models
{
    public class Race
    {
        public int Id { get; set; }

        public int Mask { get; set; }

        public string Side { get; set; }

        public string Name { get; set; }
    }
}
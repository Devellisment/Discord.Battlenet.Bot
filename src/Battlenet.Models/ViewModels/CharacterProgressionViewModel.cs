using System.Collections.Generic;

namespace Battlenet.Models
{
    public class CharacterProgressionViewModel
    {
        public IEnumerable<Raid> Raids { get; set; }
    }
}
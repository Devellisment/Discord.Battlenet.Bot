namespace Battlenet.Models
{
    public class CharacterItemViewModel
    {
        public int AverageItemLevel { get; set; }

        public int AverageItemLevelEquipped { get; set; }
    }
}
using Battlenet.Client;
using Microsoft.Extensions.Options;
using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using Battlenet.Models;

namespace Discord.Battlenet.Bot
{
    public class BattlenetBot : IBattlenetBot
    {
        private DiscordClient _discordClient;
        private IBattlenetClient _battlenetClient;
        private string _discordBotToken;
        private string _defaultWowRealm;

        // Command Constants
        private const string _progressCommand = "!progress";
        private const string _guildCommand = "!guild";

        // List
        private List<string> _availableCommands = new List<string>()
        {
            _progressCommand,
            _guildCommand
        };

        public BattlenetBot(
            IOptions<DiscordBattlenetBotOptions> discordOptions,
            IBattlenetClient battlenetClient
        )
        {
            if (discordOptions == null)
            {
                throw new ArgumentNullException(nameof(discordOptions));
            }

            if (string.IsNullOrEmpty(discordOptions.Value.DiscordBotToken))
            {
                throw new ArgumentNullException(nameof(discordOptions.Value.DiscordBotToken));
            }

            if (battlenetClient == null)
            {
                throw new ArgumentNullException(nameof(battlenetClient));
            }

            _battlenetClient = battlenetClient;
            _discordClient = new DiscordClient();
            _discordBotToken = discordOptions.Value.DiscordBotToken;
            _defaultWowRealm = discordOptions.Value.DefaultWoWRealm;
        }

        public async Task StartService()
        {
            // Register a Message Recieved event
            _discordClient.MessageReceived += async (s, e) =>
           {
               foreach (var commandCheck in _availableCommands)
               {
                   if (e.Message.Text.StartsWith(commandCheck) && e.Message.Text.Length > commandCheck.Length)
                   {
                       var command = e.Message.Text.Substring(0, e.Message.Text.IndexOf(" "));

                        // Remove command, trim
                        string argStr = e.Message.Text.Replace(command, "").Trim();

                       string[] arguments = argStr.Split(' ');

                       string message = string.Empty;

                       switch (command)
                       {
                           case _progressCommand:
                               message = await ProgressCommand(arguments);
                               break;
                           case _guildCommand:
                               message = await GuildCommand(arguments);
                               break;
                       }

                       await e.Message.Channel.SendMessage(message);
                   }
               }
           };

            // Connect to Discord
            _discordClient.ExecuteAndWait(async () =>
            {
                await _discordClient.Connect(_discordBotToken, TokenType.Bot);
            });

            throw new NotImplementedException();
        }

        private async Task<string> ProgressCommand(string[] args)
        {
            if(args.Length == 0 || args.Length > 2)
            {
                return @"Invalid argument format. Available:
                    !progress character_name
                    !progress character_name realm_name
                ";
            }

            string charName = args[0];
            string realm = _defaultWowRealm;
            
            if(args.Length == 2)
            {
                realm = args[1];
            }

            Character character = await _battlenetClient.GetCharacterProfileAsync(realm, charName, new List<CharacterFields>(){ CharacterFields.progression, CharacterFields.items });
            
            if(character == null)
            {
                return string.Format("Unable to find character [{0}] from realm {1}.", charName, realm);
            }

            string raids = string.Empty;

            foreach(var raid in character.Progression.Raids)
            {
                List<string> raidFilter = new List<string>()
                {
                    "The Emerald Nightmare"
                };

                if(raidFilter.Contains(raid.Name))
                {
                    raids += string.Format("\t{0}: LFR[{1}] N[{2}] H[{3}] M[{4}]\n", raid.Name, raid.Lfr, raid.Normal, raid.Heroic, raid.Mythic);
                }
            }

            string message = string.Format("**{0}** [{1} {2}]\n```\nAvg iLvl:{3}\nEquipped iLvl: {4}\nRaids:\r{5}\n```", character.Name, character.Level, character.Class, character.Items.AverageItemLevel, character.Items.AverageItemLevelEquipped, raids);
            
            return message;
        }

        private async Task<string> GuildCommand(string[] args)
        {
            return "Hey2";
        }
    }
}
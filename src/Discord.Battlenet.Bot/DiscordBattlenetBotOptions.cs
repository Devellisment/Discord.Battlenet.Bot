namespace Discord.Battlenet.Bot
{
    public class DiscordBattlenetBotOptions
    {
        public string DiscordBotToken { get; set; }

        public string DefaultWoWRealm { get; set; }
    }
}
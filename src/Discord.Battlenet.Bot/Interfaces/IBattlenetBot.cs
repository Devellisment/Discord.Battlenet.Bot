using System.Threading.Tasks;

namespace Discord.Battlenet.Bot
{
    public interface IBattlenetBot
    {
        Task StartService();
    }
}
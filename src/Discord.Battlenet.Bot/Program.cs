using System;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;

namespace Discord.Battlenet.Bot
{
    public class Program
    {
        static void Main(string[] args)
        {
            Startup startup = new Startup();
            IServiceProvider services = startup.ConfigureServices();

            IBattlenetBot program = services.GetRequiredService<IBattlenetBot>();
            
           Task.WaitAll(program.StartService()); 
        }
    }
}
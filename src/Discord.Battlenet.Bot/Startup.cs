using System;
using Battlenet.Client;
using Microsoft.Extensions.DependencyInjection;

namespace Discord.Battlenet.Bot
{
    public class Startup
    {
        public IServiceProvider ConfigureServices()
        {
            IServiceCollection services = new ServiceCollection();

            services.AddOptions();

            services.Configure<DiscordBattlenetBotOptions>( options => 
            {
                options.DiscordBotToken = Environment.GetEnvironmentVariable("DISCORD_BOT_TOKEN");
                options.DefaultWoWRealm = Environment.GetEnvironmentVariable("DISCORD_DEFAULT_WOW_REALM");
            });

            services.Configure<BattlenetClientOptions>( options => 
            {
                options.ApiKey = Environment.GetEnvironmentVariable("BATTLE_NET_API_KEY");
                options.BattlenetEndpoint = Environment.GetEnvironmentVariable("BATTLE_NET_ENDPOINT") ?? "https://us.api.battle.net/wow/";
                options.Locale = Environment.GetEnvironmentVariable("BATTLE_NET_LOCALE") ?? "en-us";
            });

            services.AddTransient<IBattlenetBot, BattlenetBot>();
            services.AddTransient<IBattlenetClient, BattlenetClient>();
            
            return services.BuildServiceProvider();
        }
    }
}